## Kernel Config Notes for Lenovo Yoga 2 11" Model 20332 ##


### About ###

I HAVE RETIRED THIS LAPTOP AND THUS NO LONGER UPDATING THE CONFIGS.
YOU'LL HAVE TO WORK OUT ANY OTHER ISSUES/MISSING HARDWARE ON YOUR OWN!!

That being said, before I totally close the books on this laptop, I will
remove the less complete version of the two configs currently included
(I beleive I was actively using 4.12 the last time I messed with this
machine, but it very well may have been 4.8, so double check). Also, I
still own the laptop, so if you post in the forum thread (link below),
I'm still happy to try to help as much as I can.

These files are based on Pappy's Preconfigs (link below).

Please refer to the READEME.md file in the top level of this git repo,
as it contains additional important notes and credits!


### Status ###

Tested:

 - It boots, runs Xorg/xdm/qtile/Firefox/Youtbe
 - All devices in 'lspci -k' now appear to associate with a driver
 and/or module! (I've had issues with lpc_ich not loading under some
 circumstances...trying to figure it out still)
 - Wifi (tested w/ wpa_supplicant on a standard WPA2_Personal router)
 - USB (tested a flash drive in both ports)
 - Graphics (3D acceleration not tested)
 - Card reader (monted an SD card)
 - Touchscreen (at least minimal functionality)
 - Audio
 - touchpad
 - camera

Not Tested:

 - bluetooth
 - special keyboard functions, orientation, "tablet mode" (needs
 research).

Note: there may or may not be other features enabled, such as iptables
and ACPI related stuff...You should double check everything!


### In /etc/portage/make.conf ###

    VIDEO_CARDS="intel i915"
    INPUT_DEVICES="evdev"
    CPU_FLAGS_X86="mmx mmxext popcnt sse sse2 sse3 sse4_1 sse4_2 ssse3"


### Links ###

 - [Gentoo Forum Post](https://forums.gentoo.org/viewtopic-p-8095934.html)
 - [Gentoo Wiki Page](https://wiki.gentoo.org/wiki/Lenovo_Yoga_2_11-inch)
 - [BitBucket Repo](https://bitbucket.org/experimentfailed/gentoo-kernel-configs)
 - [Pappy's Preconfigs](https://forums.gentoo.org/viewtopic-t-1051430.html)
