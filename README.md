# README #

THESE FILES ARE PROVIDED AS-IS WITHOUT ANY WARRANTY, EXPRESSED NOR
IMPLIED.

**USE AT YOUR OWN RISK!**


### About ###

Here are some kernel configurations for various hardware I own
(motherboards+CPU cobos, laptops, etc). They are based on Pappy's
Preconfigs (link below) and sys-kernel/kergen with additional manual
configuration.

These files may be in any state of completion at any time. I will
provide a NOTES.md file for each one to explain as much as I know about
what is working/configured/tested and what is not. You may also find
output from various commands for your consumption (most were run from
systemrescuecd or a Saybayon live image prior to install on that
system).

I set my systems up per the recommended defaults of the Gentoo Handbook.
This implies sysklogd, cronie, ext4, OpenRC, GRUB 2, and etc. I usually
use gentoo-sources, but will note any variance in each config's notes.

Currently included:

 - Lenovo Yoga 2 11", model number 20332 [RETIRED]
 - MSI GT72S-G220 gaming laptop
 - ASUS Maximus VIII Extreme+i7-6700K+GTX960 workstation

Coming soon:

 - HP Pavilion 15z-cd000 laptop


### How to use ###

Briefly, just clone this repo, then replace /usr/src/linux/.config with
the file for the appropriate kernel version and hardware model.

For example:

    mkdir ~/gentoo-kernel-configs
    git clone https://experimentfailed@bitbucket.org/experimentfailed/gentoo-kernel-configs.git ~/gentoo-kernel-configs
    cp ~/gentoo-kernel-configs/<hardware>/config_X.XX /usr/src/linux/.config

Then build as you normally would. Don't forget to build/rebuild your
initramfs and GPU drivers as applicable!

Kernel updating works well with the 'make oldconfig' command. It's not
likely I'll remember to keep these up to date all the time :)


### Links ###

 - [BitBucket Repo](https://bitbucket.org/experimentfailed/gentoo-kernel-configs/src/master/)
 - [Pappy's Preconfigs](https://forums.gentoo.org/viewtopic-t-1051430.html)
