## Kernel Config Notes for ASUS Maximus VIII Extreme+i7-6700k+GTX969 ##


### About ###

This configuration was created based on a Pappy Preconfig (link below),
and sys-kernel/kergen. It is for/from a machine that I use pretty much
daily, so whenever I need some feature working that I haven't already
configured, I make it so (wherever possible...so far so good) and try to
remember to update this repo.

Please refer to the READEME.md file in the top level of this git repo,
as it contains additional important notes and credits!


### Status ###

Tested:

 - graphics using Nvidia drivers
 - USB (thumbdrives, keyboard, mouse)
 - Internal HD Audio
 - SATA (it boots)

Not Tested:

 - WiFi
 - Most everything not listed above

Other Features Enabled:

 - iptables stuff (per Gentoo wiki)
 - 1000hz timer frequency
 - ACPI related stuffs (CPU Freq Scaling default set to 'ondemand')
 - CONFIG_SCHED_SMT, CONFIG_SCHED_MC, and CONFIG_SCHED_MC_PRIO
 - Processor Family set to Core 2/newer Xeon
 - Maximum Number of CPUs set to 8
 - Default frequency governor set to powersave

Note: This may or may not be an exhaustive list!


### In /etc/portage/make.conf ###

    CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext popcnt sse sse2 sse3 sse4_1 sse4_2 s$
    VIDEO_CARDS="nvidia"


### Links ###

 - [BitBucket Repo](https://bitbucket.org/experimentfailed/gentoo-kernel-configs)
 - [Pappy's Preconfigs](https://forums.gentoo.org/viewtopic-t-1051430.html)

