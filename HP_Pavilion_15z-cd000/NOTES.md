## Kernel Config Notes for MSI GT72S-G220 Gaming Laptop ##


### About ###

This configuration was created based on a Pappy Preconfig (link below),
and sys-kernel/kergen. It is for/from a laptop that I use pretty much
daily, so whenever I need a bit of hardware working that I haven't
already configured, I make it so (wherever possible...so far so good)
and try to remember to update this repo.

Please refer to the READEME.md file in the top level of this git repo,
as it contains additional important notes and credits!


### Status ###

Tested:

 - graphics (internal display)
 - touchpad (as far as basic functionality)
 - USB (thumbdrives)
 - Internal Audio (output)

Not Tested:

 - RGB lighting (may never bother trying)
 - Thunderbolt (enabled; should work in theory)
 - VT-d (IOMMU enabled; also should work)
 - Card reader
 - HDMI
 - bluetooth (only as far as pairing)
 - Camera
 
Other Features Enabled:

 - iptables stuff (per Gentoo wiki)
 - 1000hz timer frequency
 - ACPI related stuffs (CPU Freq Scaling default set to 'ondemand')
 - CONFIG_SCHED_SMT, CONFIG_SCHED_MC, and CONFIG_SCHED_MC_PRIO
 - Maximum Number of CPUs set to 4

Note: This may or may not be an exhaustive list!


### In /etc/portage/make.conf ###

    CPU_FLAGS_X86="aes avx avx2 f16c fma3 fma4 mmx mmxext pclmul popcnt sse sse2 sse3 sse$
    VIDEO_CARDS="amdgpu radeonsi"
    INPUT_DEVICES="evdev synaptics"


### Links ###

 - [BitBucket Repo](https://bitbucket.org/experimentfailed/gentoo-kernel-configs)
 - [Pappy's Preconfigs](https://forums.gentoo.org/viewtopic-t-1051430.html)
