## Kernel Config Notes for MSI GT72S-G220 Gaming Laptop ##


### About ###

This configuration was created based on a Pappy Preconfig (link below),
and sys-kernel/kergen. It is for/from a laptop that I use pretty much
daily, so whenever I need a bit of hardware working that I haven't
already configured, I make it so (wherever possible...so far so good)
and try to remember to update this repo.

Please refer to the READEME.md file in the top level of this git repo,
as it contains additional important notes and credits!


### Disks ###

Fuse is enabled. I primarily use ext4, but seems like I haven't had any
issues mounting whatever I throw at it. Just note, I probably haven't
thrown everything at it (maybe not even FAT/FAT32/NTFS)

Software RAID and bcache are enabled in these configs. If you want to
use RAID for your root partition, make sure to build an initramfs. To do
so, after building the kernel, I just use genkernel as such:

	genkernel initramfs --dmraid

In /etc/default/grub, before running grub-mkconfig, I also set

    GRUB_CMDLINE_LINUX="domdadm"

Finally, update grun (make sure your boot partition is mounted):

    grub-mkconfig -o /boot/grub/grub.cfg

One setup you may want to consider (and this is how I have mine) is to
create 2 RAID partitions on each SSD, to make 2 RAID0 sets out of them.
Use one set for your root partition and the other set as a bcache to the
laptop's 1tb mechanical drive. The user experience is phenomenal!

FWIW, here are the /dev locations for my system:

	/dev/cdrom -> /dev/sr0 -- The optical drive
	/dev/mmcblk0 -- So far, every time I instert an SD card, this is it.
	With multiple partitions, I get mmcblk0p1, mmcblk0p2, etc
	/dev/nvme0n1 -- SSD 0 (again, partitions append like nvme0n1p1, etc)
	/dev/nvme1n1 -- SSD 1 "

	Note: I do not know how the SSDs correspond to the phsyical NVMe
	connectors on the motherboard


### Status ###

Tested:

 - graphics using Nvidia drivers
 - touchpad (as far as basic functionality)
 - USB (thumbdrives, keyboard, mouse, class-compliant audio, etc)
 - SSDs (NVME)
 - Internal HD Audio
 - Triple monitors (HDMI + DisplayPort + built-in display)
 - optical drive (tested with video DVD)
 - Camera
 - Card reader
 - bluetooth (only as far as pairing)

Not Tested:

 - RGB lighting (may never bother trying)
 - Thunderbolt (enabled; should work in theory)
 - VT-d (IOMMU enabled; also should work)

Other Features Enabled:

 - USB Audio (tested with UMC1820 class-compliant device)
 - MIDI I/O (tested with M-AUDIO Keystation 61, Behringer BCF/BCR
 - KVM/Qemu related stuff (per Gentoo wiki; Got a Windows guest going)
 - iptables stuff (per Gentoo wiki)
 - DisplayLink (get green screen; haven't verified full functionality)
 - Software RAID (see above)
 - Bcache (see above)
 - 1000hz timer frequency
 - ACPI related stuffs (CPU Freq Scaling default set to 'powersave')
 - CONFIG_SCHED_SMT, CONFIG_SCHED_MC, and CONFIG_SCHED_MC_PRIO
 - Processor Family set to Core 2/newer Xeon
 - Maximum Number of CPUs set to 8
 - I usually set CONFIG_PREEMPT (real-time kernel)

Note: This may or may not be an exhaustive list!


### In /etc/portage/make.conf ###

    CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext popcnt sse sse2 sse3 sse4_1 sse4_2 ssse3"
    VIDEO_CARDS="nvidia"
    INPUT_DEVICES="evdev synaptics"


### Links ###

 - [Gentoo Forum Post](https://forums.gentoo.org/viewtopic-p-8109222.html#8109222)
 - [Gentoo Wiki Page](https://wiki.gentoo.org/wiki/MSI_GT72S-G220)
 - [BitBucket Repo](https://bitbucket.org/experimentfailed/gentoo-kernel-configs)
 - [Pappy's Preconfigs](https://forums.gentoo.org/viewtopic-t-1051430.html)
 - [Performance Issue - Solved!](https://forums.gentoo.org/viewtopic-t-1072464.html)
